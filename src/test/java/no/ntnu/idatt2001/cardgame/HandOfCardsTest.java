package no.ntnu.idatt2001.cardgame;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {
    private HandOfCards hand;

    @Before
    public void initialize() {
        hand = new HandOfCards();
    }

    /**
     * Creates 5 cards of same suit.
     */
    private ArrayList<PlayingCard> createFlush() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 0; i<5; i++) {
            cards.add(new PlayingCard('H', i));
        }
        return cards;
    }

    /**
     * Creates 4 "normal" cards (not flush or s12).
     */
    private ArrayList<PlayingCard> createGeneric4() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        char[] suits = {'H','S','C','D'};
        for (int i = 1; i<2; i++) {
            for (int j = 0; j<4; j++) {
                cards.add(new PlayingCard(suits[j],i));
            }
        }
        return cards;
    }

    /**
     * Creates 5 "normal" cards (not flush or s12).
     */
    private ArrayList<PlayingCard> createGeneric5() {
        ArrayList<PlayingCard> cards = new ArrayList<>(createGeneric4());
        cards.add(new PlayingCard('H',2));
        return cards;
    }

    @Test
    @DisplayName("isFlush returns true for flush")
    public void isFlushTest() {
        hand.addCards(createFlush());
        assertTrue(hand.isFlush());
    }

    @Test
    @DisplayName("isFlush returns false for not flush")
    public void isFlushNegativeTest() {
        hand.addCards(createGeneric4());
        assertFalse(hand.isFlush());
    }

    @Test
    @DisplayName("isQueenOfSpades returns true if hand contains s12-card")
    public void isQueenOfSpadesTest() {
        hand.addCards(createGeneric4());
        hand.addCards(Collections.singletonList(new PlayingCard('S', 12)));
        assertTrue(hand.isQueenOfSpades());
    }

    @Test
    @DisplayName("isQueenOfSpades returns false if hand does not contain s12-card")
    public void isQueenOfSpadesNegativeTest() {
        hand.addCards(createGeneric4());
        assertFalse(hand.isQueenOfSpades());
    }

    @Test
    @DisplayName("Adding too many cards throws exception")
    public void addingTooManyCards() {
        hand.addCards(createGeneric4());
        assertThrows(IllegalArgumentException.class, () -> hand.addCards(createGeneric4()));
    }

    @Test
    @DisplayName("Sum of faces returns correct sum")
    public void sumFaceTest() {
        hand.addCards(createGeneric5());
        assertEquals(6,hand.getSumFaces());
    }

    @Test
    @DisplayName("returnCards empties the hand")
    public void returnCardsTest() {
        hand.addCards(createGeneric4());
        hand.returnCards();
        assertEquals("",hand.getAsString());
    }
}
