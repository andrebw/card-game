package no.ntnu.idatt2001.cardgame;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    private DeckOfCards deck;

    @Before
    public void initialize() {
        deck = new DeckOfCards();
    }

    /**
     * Creates a number of cards. The type does not matter.
     */
    private ArrayList<PlayingCard> createCards(int num) {
        ArrayList<PlayingCard> cards = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            cards.add(new PlayingCard('H', i));
        }
        return cards;
    }

    @Test
    @DisplayName("Adding too many cards throws exception")
    public void addingTooManyCards() {
        assertThrows(IllegalArgumentException.class, () -> deck.addCards(createCards(1)));
    }

    @Test
    @DisplayName("Deck size is correct (52)")
    public void getSizeTest() {
        assertEquals(52, deck.getDeckSize());
    }

    @Test
    @DisplayName("dealHand removes and returns the correct amount of cards")
    public void dealHandTest_deckAndHandSizeAreAsExpected() {
        int handSize = deck.dealHand(5).size();
        int deckSize = deck.getDeckSize();
        assertEquals(5, handSize);
        assertEquals(47,deckSize);
    }

    @Test
    @DisplayName("The cards that are dealt are no longer in the deck")
    public void dealHandTest_dealtCardsAreNotInDeck() {
        ArrayList<PlayingCard> dealtCards = deck.dealHand(3);
        assertFalse(deck.getCardsInDeck().contains(dealtCards.get(0)));
        assertFalse(deck.getCardsInDeck().contains(dealtCards.get(1)));
        assertFalse(deck.getCardsInDeck().contains(dealtCards.get(2)));
    }

    @Test
    @DisplayName("Remove five then adding five cards will not result in an exception")
    public void dealHandThenAddSameAmount() {
        deck.dealHand(5);
        assertDoesNotThrow(() -> deck.addCards(createCards(5)));
    }
}
