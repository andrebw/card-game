module no.ntnu.idatt2001.cardgame {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt2001.cardgame to javafx.fxml;
    exports no.ntnu.idatt2001.cardgame;
}