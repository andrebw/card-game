package no.ntnu.idatt2001.cardgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * JavaFX App
 * @author Andreas B. Winje
 * @version 08-04-2021
 */
public class App extends Application {
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 650, 420);
        setup(stage);
    }

    public void setup(Stage stage) {
        stage.setTitle("Card Game");
        stage.setScene(scene);
        stage.setWidth(700);
        stage.setMinWidth(650);
        stage.setMinHeight(470);
        stage.setMaxWidth(800);
        stage.setMaxHeight(500);
        stage.setResizable(true);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Loads a .fxml-file by a file-name.
     * @param fxml name of the file.
     * @return the loaded .fxml-file.
     * @throws IOException if file can not be loaded.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        return FXMLLoader.load(App.class.getResource("/" + fxml + ".fxml"));
    }

    public static void main(String[] args) {
        launch();
    }
}