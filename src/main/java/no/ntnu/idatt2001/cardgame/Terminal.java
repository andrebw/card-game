package no.ntnu.idatt2001.cardgame;

import javafx.scene.control.Label;

/**
 * A message-terminal that displays messages in a javafx-label.
 * I simply had some extra time to experiment, so I made this. It really has nothing to do with the assignment.
 * @author Andreas B. Winje
 * @version 08-04-2021
 */
public class Terminal {

    private String messageLines;
    private final Label messageLabel;
    private final int maxLines;

    /**
     * Constructor creates a Terminal-object by a javafx-label and a size (how many maximum lines).
     *
     * @param label a javafx-label to display on.
     * @param size maximum lines of the terminal.
     */
    public Terminal(Label label, int size) {
        this.messageLabel = label;
        this.maxLines = size;
        messageLines = "";
    }

    /**
     * Adds a message to the messageLines. Cuts of the first lines if there are too many lines.
     *
     * @param message to add.
     */
    public void append(String message) {
        messageLines = getLastLines() + message;
        messageLabel.setText(messageLines);
    }

    /**
     * Gets the last n lines of the message. If the total lines of the message is less than the max, it just returns the
     * message with a new line.
     *
     * @return last n lines of the message.
     */
    public String getLastLines() {
        String[] strings = messageLines.split("\n");
        if (strings.length < maxLines) {
            return messageLines + "\n";
        }
        String result = "";
        //Adds the lines from a index to the last line so that the total lines amounts to maxLines.
        for (int i = strings.length - maxLines + 1; i<strings.length; i++) {
            result += strings[i] + "\n";
        }
        return result;
    }

    /**
     * Clears the terminal.
     */
    public void clear() {
        messageLines = "";
        messageLabel.setText(messageLines);
    }
}
