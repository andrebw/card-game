package no.ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * A deck of cards that can hold up to 52 playing cards. Can also deal a number of random cards.
 * @author Andreas B. Winje
 * @version 08-04-2021
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' }; //the type/suit of the card
    ArrayList<PlayingCard> cardsInDeck;

    /**
     * Constructor that initializes a DeckOfCards and fills it with a standard deck of cards.
     */
    public DeckOfCards() {
        cardsInDeck = new ArrayList<>(52);
        fillDeckWithCards();
    }

    /**
     * Deals a number of cards: adds a number of random cards to a list, removes said cards from the deck and
     * returns the list.
     *
     * @param n how many cards to deal (typically five).
     * @return n random cards from the deck.
     * @throws IllegalArgumentException if one tries to deal less than zero or more than 52 cards.
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {
        if (n>52 || n<0) { throw new IllegalArgumentException("Invalid amount of cards to deal"); }
        Random rnd = new Random();
        ArrayList<PlayingCard> dealtCards = new ArrayList<>();
        while (n>0) {
            int index = rnd.nextInt(cardsInDeck.size());
            dealtCards.add(cardsInDeck.get(index));
            cardsInDeck.remove(index);
            n--;
        }
        return dealtCards;
    }

    /**
     * Adds a collection of cards to the deck.
     *
     * @param cards to be added to the deck.
     * @throws IllegalArgumentException if current deck size and number of cards added exceeds 52 cards.
     */
    public void addCards(Collection<PlayingCard> cards) throws IllegalArgumentException {
        if (cardsInDeck.size() + cards.size() > 52) {
            throw new IllegalArgumentException("Too many cards added");
        }
        cardsInDeck.addAll(cards);
    }

    /**
     * Fills the deck with cards. Should only be called in the constructor.
     */
    private void fillDeckWithCards() throws IllegalArgumentException {
        if (cardsInDeck.size() > 0) {
            throw new IllegalArgumentException("The deck is not empty");
        }
        for (int i = 1; i<=13; i++) {
            for (int j = 0; j<4; j++) {
                cardsInDeck.add(new PlayingCard(suit[j],i));
            }
        }
    }

    //getter
    public ArrayList<PlayingCard> getCardsInDeck() {
        return cardsInDeck;
    }

    //getter
    public int getDeckSize() {
        return cardsInDeck.size();
    }


}
