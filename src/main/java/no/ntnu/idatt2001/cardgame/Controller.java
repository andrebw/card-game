package no.ntnu.idatt2001.cardgame;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.ArrayList;

/**
 * Controller that controls the scene and all the buttons and information in it.
 * @author Andreas B. Winje
 * @version 08-04-2021
 */
public class Controller {

    //Labels:
    @FXML private Label deckSizeLabel, handsDealtLabel, isFlushLabel, isQueenOfSpadesLabel, heartsLabel,
            sumFacesLabel, cardsLabel, messageWindow;
    //ImageViews:
    @FXML private ImageView imageViewA,imageViewB, imageViewC, imageViewD, imageViewE;
    //Buttons:
    @FXML private Button returnButton;

    private final ArrayList<ImageView> imageViewList = new ArrayList<>();

    private final Image defaultImage = getImage("blue_back"); //the default image, if there are no cards in the hand.

    private int handsDealt = 0; //how many cards have been dealt.
    private DeckOfCards deck;
    private HandOfCards hand;
    private Terminal terminal;

    /**
     * Initializes to app. Sets up a deck, hand, message-terminal and the ImageViews.
     * (ImageView-objects did not work with normal Arrays for some reason)
     */
    @FXML
    public void initialize() {
        terminal = new Terminal(messageWindow, 10);
        terminal.append("Welcome!");
        deck = new DeckOfCards();
        hand = new HandOfCards();
        imageViewList.add(imageViewA);
        imageViewList.add(imageViewB);
        imageViewList.add(imageViewC);
        imageViewList.add(imageViewD);
        imageViewList.add(imageViewE);
        updateInfo();
    }

    /**
     * What happens when the 'Deal hand'-button is pressed.
     */
    @FXML
    public void onDealHand() {
        dealHand();
        handsDealt++;
        returnButton.setDisable(false);
        updateInfo();
        System.out.print(hand.getAsString() + " ");
        System.out.println(hand.getSumFaces());
    }

    /**
     * What happens when the 'Return cards'-button is pressed.
     */
    @FXML
    public void onReturnCards() {
        returnCards();
        returnButton.setDisable(true);
        updateInfo();
    }

    /**
     * Returns the cards in the hand to the deck.
     */
    private void returnCards() {
        deck.addCards(hand.returnCards());
    }

    /**
     * Updates all the information on the screen and checks flush-/s12-events.
     */
    @FXML
    private void updateInfo() {
        updateLabels();
        updateImages();
        checkFlushAndQueen();
    }

    /**
     * Updates all the card-images. It loads the images with the same name as the cards in the hand. If the hand is empty, it loads
     * the default image (back-side of a card).
     */
    private void updateImages() {
        if(hand.getCardsInHand().isEmpty()) {
            for (ImageView view : imageViewList) {
                view.setImage(defaultImage);
            }
        }
        else {
            for (int i = 0; i<imageViewList.size(); i++) {
                imageViewList.get(i).setImage(getImage(hand.getCardsInHand().get(i).getAsStringReversed()));
            }
        }
    }

    /**
     * Creates a new image from a name and returns it.
     *
     * @param name of the image.
     * @return a new image.
     */
    private Image getImage(String name) {
        return new Image(getPath(name));
    }

    /**
     * Gets the path to a .png-file from a filename. Used for finding image-paths.
     *
     * @param name of the .png-file.
     * @return the path to the .png-file.
     */
    private String getPath(String name) {
        return getClass().getResource("/PNG/" + name + ".png").toString();
    }

    /**
     * Updates all the labels.
     */
    private void updateLabels() {
        format(isFlushLabel, hand.isFlush() ? "Yes!" : "No");
        format(isQueenOfSpadesLabel,hand.isQueenOfSpades() ? "Yes" : "No");
        format(heartsLabel,hand.getHearts().isEmpty() ? "None" : hand.getHeartsAsString());
        format(sumFacesLabel,hand.getSumFaces());
        cardsLabel.setText(hand.getAsString());
        format(handsDealtLabel,handsDealt);
        format(deckSizeLabel,deck.getDeckSize());
    }

    /**
     * Formats the labels so that only the text after the ": " is set. This way the descriptive part of the label will not disappear
     * when setting the text.
     *
     * @param label the label to be changed
     * @param text the text to set the label to
     */
    private void format(Label label, Object text) {
        label.setText(label.getText().split(": ")[0].concat(": " + text));
    }

    /**
     * Checks if a flush- or queenOfSpades-event has happened. If so, it prints to the terminals (the custom one on screen
     * and the computers terminal) how many hands dealt at this point.
     */
    private void checkFlushAndQueen() {
        if (hand.isFlush()) {
            System.out.println("You got a flush (" + hand.getAsString() + ") " + "after " + handsDealt + " hands " +
                    "dealt!");
            terminal.append("You got a flush! (" + handsDealt + ")");
        }
        if (hand.isQueenOfSpades()) {
            System.out.println("You got a queen of spades! Hands dealt: " + handsDealt);
            terminal.append("You got a queen of spades! (" + handsDealt + ")");
        }
    }

    /**
     * Exits the program when the 'Exit'-button is pressed.
     */
    @FXML
    public void onExit() {
        System.out.println("Exiting program...");
        System.exit(0);
    }

    /**
     * Gets cards from the deck and adds them to the hand.
     * Returns the cards from the hand first, so it doesn't accumulate in the hand.
     */
    public void dealHand() {
        deck.addCards(hand.returnCards());
        hand.addCards(deck.dealHand(5));
    }
}
